# Televisión Chilena en Kodi

Fuente de video de canales de televisión Chilena para [Kodi](https://kodi.tv/about), alojada en http://jacobsen.cl/kodi.
Actualizaciones son bienvenidas mediante pull request.

Link: 
* ~~https://jacobsen.cl/~christian/tv-online-en-chile-en-kodi-xbmc/~~ 
* https://jacobsen.cl/~christian/tv-kodi-chile/

